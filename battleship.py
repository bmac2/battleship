# A board is a list of rows containing cells with an 'X' for each ship
board = [
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    
]

# dictionary to translate letters to the corresponding number
letters_to_numbers = {
    'A': 0,
    'B': 1,
    'C': 2,
    'D': 3,
    'E': 4,
    'F': 5,
    'G': 6,
    'H': 7,
    'I': 8,
    'J': 9,
    
}


# writing this as a function to prevent repeating code
def ask_user_for_board_position():
    column = input("column (A to J):")
    while column not in "ABCDEFGHIJ":
        print("That column is wrong! It should be A, B, C, D, E, F, G, H, I, J")
        column = input("column (A to J):")

    row = input("row (0 to 9):")
    while row not in "0123456789":
        print("That row is wrong! it should be 0, 1, 2, 3, 4, 5, 6, 7, 8, 9")
        row = input("row (0 to 9):")

    return int(row) - 1, letters_to_numbers[column]


def print_board(board):
    # Display the board
    print("   A   B   C   D   E   F   G   H   I   J ")
    print(" + - + - + - + - + - + - + - + - + - + - +")
    row_number = 1
    for row in board:
        print("%d|%s|" % (row_number, "|".join(row)))
        print(" + - + - + - + - + - + - + - + - + - + - +")
        row_number = row_number + 1


# We want 5 battleships, so we use a for loop to ask for a ship 5 times!
for n in range(5):
    print("Where do you want ship ", n + 1, "?")
    row_number, column_number = ask_user_for_board_position()

    # Check that there are no repeats
    if board[row_number][column_number] == ' X ':
        print("That spot already has a battleship in it!")

    board[row_number][column_number] = ' X '
    print_board(board)


# Now clear the screen, and the other player starts guessing
print("\n"*50)

guesses_board = [
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    ['   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ', '   '],
    
]


# Keep playing until we have 5 right guesses
guesses = 0
while guesses < 5:
    print("Guess a battleship location")
    row_number, column_number = ask_user_for_board_position()

    if guesses_board[row_number][column_number] != '   ':
        print("You have already guessed that place!")
        continue

    if board[row_number][column_number] == ' X ':
        print("HIT! YOU SANK MY BATTLESHIP!!!!")
        guesses_board[row_number][column_number] = ' X '
        guesses = guesses + 1
    else:
        guesses_board[row_number][column_number] = ' M '
        print("MISS! LOOOOSER!!!!")

    print_board(guesses_board)
print("GAME OVER!")